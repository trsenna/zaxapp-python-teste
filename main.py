from typing import List, Dict, Optional


class Pedido:
    def __init__(self, nome: str, preco: float) -> None:
        self.nome = nome
        self.preco = preco


class Loja:
    def __init__(self, nome: str, comissao: float, pedidos: List[Pedido] = []) -> None:
        self.nome = nome
        self.comissao = comissao
        self.pedidos = pedidos


class Motoboy:
    def __init__(self, nome: str, taxa: float, exclusividade: Optional[Loja] = None) -> None:
        self.nome = nome
        self.taxa = taxa
        self.exclusividade = exclusividade

    def disponibilidade_para(self, loja: Loja) -> bool:
        return self.exclusividade == None or self.exclusividade == loja


class Coleta:
    def __init__(self, motoboy: Motoboy):
        self.motoboy: Motoboy = motoboy
        self.pedidos: Dict[Loja, List[Pedido]] = {}
        self.total: float = motoboy.taxa

    def adiciona_item(self, pedido: Pedido, loja: Loja) -> bool:
        if not self.motoboy.disponibilidade_para(loja):
            return False
        if loja not in self.pedidos:
            self.pedidos[loja] = []
        self.pedidos[loja].append(pedido)
        self.total += pedido.preco * (loja.comissao / 100)
        return True

    def prioridade(self):
        if self.motoboy.taxa == self.total:
            return -1 if self.motoboy.exclusividade else 0
        return self.total

    def __str__(self) -> str:
        relatorio = f"O motoboy {self.motoboy.nome} vai entregar {len(self.pedidos)} pedido(s)"
        relatorio += f"\nLoja(s) atendida(s) nesta coleta: {', '.join([loja.nome for loja in self.pedidos.keys()])}"
        relatorio += f"\nMotoboy irá receber: R$ {self.total}"
        relatorio += f"\n --- "
        return relatorio


def priorizar_coletas(coletas: List[Coleta]) -> List[Coleta]:
    return sorted(coletas, key=Coleta.prioridade)


def despachar(lojas: List[Loja], motoboys: List[Motoboy]) -> List[Coleta]:
    coletas: List[Coleta] = []

    # inicia coletas para cada motoboy
    for motoboy in motoboys:
        coletas.append(Coleta(motoboy))

    for loja in lojas:
        for pedido in loja.pedidos:
            # prioriza coletas considerando preco e motoboy disponível
            priorizadas = sorted(coletas, key=Coleta.prioridade)
            for coleta in priorizadas:
                # tenta adicionar pedido à coleta
                if coleta.adiciona_item(pedido, loja):
                    break

    return coletas


def resumo(coletas: List[Coleta], motoboy: Motoboy = None) -> None:
    for coleta in coletas:
        if motoboy == None:
            print(coleta)
        elif motoboy == coleta.motoboy:
            print(coleta)


if __name__ == '__main__':
    lojas = [
        Loja("loja #1", 5, pedidos=[
            Pedido("loja #1 | pedido #1", 50),
            Pedido("loja #1 | pedido #2", 50),
            Pedido("loja #1 | pedido #3", 50),
        ]),
        Loja("loja #2", 5, pedidos=[
            Pedido("loja #2 | pedido #1", 50),
            Pedido("loja #2 | pedido #2", 50),
            Pedido("loja #2 | pedido #3", 50),
            Pedido("loja #2 | pedido #4", 50),
        ]),
        Loja("loja #3", 15, pedidos=[
            Pedido("loja #3 | pedido #1", 50),
            Pedido("loja #3 | pedido #2", 50),
            Pedido("loja #3 | pedido #3", 100),
        ]),
    ]

    motoboys = [
        Motoboy("motoboy #1", 2),
        Motoboy("motoboy #2", 2),
        Motoboy("motoboy #3", 2),
        Motoboy("motoboy #4", 2, exclusividade=lojas[0]),
        Motoboy("motoboy #5", 3),
    ]

    coletas = despachar(lojas=lojas, motoboys=motoboys)
    # imprime coleta do motoboy #4
    resumo(coletas=coletas, motoboy=motoboys[3])
    # imprime todas coletas
    resumo(coletas=coletas)
